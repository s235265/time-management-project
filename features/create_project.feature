# Created by nicho at 08/04/2024
Feature: Creation and management of projects
  # Enter feature description here

  Scenario: A project is created succesfully
    Given an employee is logged in
    And there is no project with the name "Project A"
    When a project with the name "Project A" is created
    Then the project with the name "Project A" should exist

  Scenario: A project is created unsuccesfully:
    Given an employee is logged in
    And there is a project with the name "Project A"
    When a project with the name "Project A" is created
    Then the exception "Project name already exists" is thrown
    
  Scenario: A project is deleted from the system succesfully
    Given a project with the name "Project" exists
    When the project is removed from the system
    Then the project should be removed from the system
    
  Scenario: A project is deleted unsuccesfully
    Given there is no project with the name "Project"
    When the project is removed from the system
    Then the exception "That project doesn't exist" is thrown

  Scenario: A project has an activity deleted
    Given a project exists
    And an activity exists
    And the activity is assigned to the project
    When the activity is removed from the project
    Then the activity should not be in the project