
  Feature: Select employee

    Scenario: Select employee successfully
      Given an employee exists with the id "huba"
      And an employee exists with the id "hubs"
      And the selected employee is "huba"
      When I select the employee with the id "hubs"
      Then the selected employee is set to "hubs"

    Scenario: Select employee with invalid id
        Given an employee exists with the id "huba"
        And the selected employee is "huba"
        When I select the employee with the id "hubi"
        Then the exception "No employee with that ID exists" is thrown
