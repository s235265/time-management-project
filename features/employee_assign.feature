Feature: Assign employee to activity

  Scenario: Assign employee to activity
    Given an activity exists
    And the activity is set to have 4.2 hours assigned to it
    And an employee exists with the id "huba"
    When the employee is assigned to the activity
    Then the activity should have the employee assigned to it
    And the employee's assigned hours should increment with 4.2
    
  Scenario: Employee is already assigned to activity
    Given an activity exists
    And an employee is assigned to the activity
    When the employee is assigned to the activity
    Then the exception "Employee already assigned to activity" is thrown

  Scenario: Employee isn't in the system and is assigned to an activity
    Given an activity exists
    And an employee doesn't exist with the id "huba"
    When the employee is assigned to the activity
    Then the exception "No employee with that ID exists" is thrown

    #Next two are related to Nicholas white box test
  Scenario: Employee id is too short and is assigned to an activity
    Given an activity exists
    When the employee with the id "hu" is assigned to the activity
    Then an AssertionError should be thrown

  Scenario: Activity doesn't exist and employee is assigned to activity through id
    Given an employee exists with the id "huba"
    When the employee with the id "huba" is assigned to the activity
    Then an AssertionError should be thrown
  
  Scenario: Employee id doesn't exist and is assigned to an activity
    Given an employee doesn't exist with the id "huba"
    And an activity exists
    When the employee with the id "huba" is assigned to the activity
    Then the exception "No employee with that ID exists" is thrown

  Scenario: Employee is assigned to activity
    Given an employee exists with the id "huba"
    And an activity exists
    When the employee with the id "huba" is assigned to the activity
    Then the employee should be assigned to activity

  Scenario: Assign employee to activity which is assigned to a project
    Given an activity exists
    And there is a project with the name "P"
    And the activity is assigned to the project
    And an employee exists with the id "huba"
    When the employee is assigned to the activity
    Then the employee should be assigned to the project
    And the activity should have the employee assigned to it

  Scenario: An employee is removed from an activity
    Given an employee exists with the id "huba"
    And an activity exists
    And the employee is assigned to the activity
    When the employee is removed from the activity
    Then the employee should be removed from the activity


