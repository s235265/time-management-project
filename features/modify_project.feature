Feature: modify project details and project leader
  
  Scenario: Setting the project leader
    Given an employee exists with the id "huba"
    And a project with the name "Project" exists
    When the project leader of the project is set to the employee
    Then the project leader of the project should be "huba"
    
  Scenario: Setting the project leader but no employee with that id exists
    Given there is no employee with the id "huba"
    And a project with the name "Project" exists
    When the project leader of the project is set to the employee
    Then the exception "No employee with that ID exists" is thrown