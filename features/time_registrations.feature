Feature: adding time registrations

  Scenario: Creating a time registration
    Given an employee exists
    And an activity exists
    When the employee registers 4.5 hours worked on the activity
    Then the registration's activity should be the activity
    And the registration's hours should be 4.5

  Scenario: Creating a time registration on a specific date
    Given an employee exists
    And an activity exists
    When the employee registers hours on the date "2024-05-02"
    Then the registration's date should be "2024-05-02"
    And the employee should have the time registration registered to him