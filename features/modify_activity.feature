Feature: Modifying activity details and assigned hours

  Scenario: Modifying activity details
    Given an activity exists
    When the activity's details are changed to "Example"
    Then the activity's details should be "Example"

  Scenario: Assign hours to activity
    Given an activity exists
    When 3.0 hours is assigned to the activity
    Then the activity should have 3.0 hours assigned to it

    # no fail cases, as employees can work on activities they arent assigned ot
  Scenario: An employee works on an activity
    Given an activity exists
    And an employee exists with the id "huba"
    And the employee is assigned to the activity
    When the employee registers 4.5 hours worked on the activity
    Then the activity should have 4.5 more hours worked on it
    And the employee should have the time registration registered to him
