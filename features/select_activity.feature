
Feature: Select activity

  Scenario: Select activity successfully
    Given an activity with the name "Pour coffee" exists
    When I select the activity with the name "Pour coffee"
    Then the selected activity is "Pour coffee"

  Scenario: Select non existing activity
    Given an activity with the name "Pour coffee" exists
    And no activity with the name "Non existing activity" exists
    And the selected activity is "Pour coffee"
    When I select the activity with the name "Non existing activity"
    Then the exception "No activity with that name exists" is thrown

  Scenario: An activity is selected
    Given an activity exists
    When the activity is selected
    Then the activity should be the selected activity
