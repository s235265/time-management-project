Feature: Select project

  Scenario: Select project successfully
    Given a project with the name "Project" is created
    And the selected project is "Pour coffee"
    When I select the project with the name "Project"
    Then the selected project is "Project"

  Scenario: Select project with invalid name
    Given a project with the name "Project" is created
    And the selected project is "Project"
    When I select the project with the name "Projecto"
    Then the exception "No project with that name exists" is thrown
