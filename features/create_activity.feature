Feature: Create activities

  Scenario: Create and assign activity to project
    Given there is a project with the name "Project A"
    And an employee is logged in
    When an activity with the name "Pour coffee" is added to the project
    Then the activity exists in the project

  Scenario: Create an activity without assigned project
    Given an employee is logged in
    When the activity with the name "Pour Coffee" is created
    Then the activity with the name "Pour Coffee" exists in the scheduling system
    And the activity should have no project assigned to it









