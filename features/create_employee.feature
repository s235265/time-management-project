Feature: Create employee
  
  Scenario: Employee is created successfully
    Given there is no employee with the id "huba"
    When an employee is created with the id "huba"
    Then the employee exists in the system
    
  Scenario: Employee already exists
    Given an employee exists with the id "huba"
    When an employee is created with the id "huba"
    Then the exception "Employee already exists" is thrown