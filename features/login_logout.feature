# Created by nicho at 08/04/2024
Feature: Employee login features
  #Description

  Scenario: Employee logs in succesfully
    Given an employee exists with the id "huba"
    When the user logs in with the id "huba"
    Then the employee with id "huba" is logged in

  Scenario: Employee id fails
    Given there is no employee with the id "huba"
    When the user logs in with the id "huba"
    Then the exception "No employee with that ID exists" is given"

  Scenario: Employee logs out
    Given an employee is logged in
    When the user logs out
    Then the user is logged out
