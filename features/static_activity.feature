Feature: Static which you can't change the worked hours on.

  Scenario: An employee works on a static activity
    Given an employee exists
    And a static activity exists
    When the employee registers 4.5 hours worked on the activity
    Then the activity should have 4.5 more hours worked on it

  Scenario: Activity cannot have its assigned hours changed
    Given a static activity exists
    When the activity is set to have 4.5 hours assigned to it
    Then the activity's assigned hours should be unchanged
