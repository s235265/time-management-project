package example.cucumber;

import example.cucumber.Helpers.ActivityHolder;
import example.cucumber.Helpers.EmployeeHolder;
import example.cucumber.Helpers.TimeRegistrationHolder;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_old.Ac;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegistrationSteps {
    private TimeRegistrationHolder timeRegistrationHolder;
    private SchedulingSystem schedulingSystem;
    private ActivityHolder activityHolder;
    private EmployeeHolder employeeHolder;

    public RegistrationSteps(TimeRegistrationHolder r, SchedulingSystem s, ActivityHolder a, EmployeeHolder e){
        timeRegistrationHolder = r;
        schedulingSystem = s;
        activityHolder = a;
        employeeHolder = e;
    }


    @When("the employee registers hours on the date {string}")
    public void theEmployeeRegistersHoursOnTheDate(String string) {
        Activity a = new Activity("test", schedulingSystem);
        Employee e = employeeHolder.getEmployee();
        TimeRegistration tr = new TimeRegistration(LocalDate.parse(string), 1300, 1500, a);
        e.addTimeRegistration(tr);
        timeRegistrationHolder.setTimeRegistration(tr);
    }

    @Then("the registration's date should be {string}")
    public void theRegistrationSDateShouldBe(String string) {
        TimeRegistration tr = timeRegistrationHolder.getTimeRegistration();
        assertEquals(LocalDate.parse(string), tr.getDate());
    }

    @Then("the registration's activity should be the activity")
    public void theRegistrationSActivityShouldBeTheActivity() {
        Activity a = timeRegistrationHolder.getTimeRegistration().getActivity();
        assertEquals(activityHolder.getActivity(), a);
    }
    @Then("the registration's hours should be {double}")
    public void theRegistrationSHoursShouldBe(Double double1) {
        TimeRegistration tr = timeRegistrationHolder.getTimeRegistration();
        assertEquals(double1, tr.getHoursWorked());
    }
}
