package example.cucumber;
import example.cucumber.Helpers.*;
import io.cucumber.java.an.E;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_old.Ac;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.*;

import java.sql.Time;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;


public class ActivitySteps {
    private ActivityHolder activityHolder;
    private ProjectHolder projectHolder;
    private SchedulingSystem schedulingSystem;
    private EmployeeHolder employeeHolder;
    private ErrorMessageHolder errorMessageHolder;
    private TimeRegistrationHolder timeRegistrationHolder;

    public ActivitySteps(ProjectHolder projectHolder, ActivityHolder activityHolder, SchedulingSystem s, EmployeeHolder e, ErrorMessageHolder errorMessageHolder, TimeRegistrationHolder t){
        this.projectHolder = projectHolder;
        this.activityHolder = activityHolder;
        this.schedulingSystem = s;
        this.employeeHolder = e;
        this.errorMessageHolder = errorMessageHolder;
        this.timeRegistrationHolder = t;
    }

    @Given("an activity exists")
    public void anActivityExists() {
        Activity activity = new Activity("Name", schedulingSystem);
        activityHolder.setActivity(activity);
        schedulingSystem.addActivity(activity);
    }

    @When("an activity with the name {string} is added to the project")
    public void an_activity_with_the_name_is_added_to_the_project(String string) {
        Project project = projectHolder.getProject();
        Activity activity = new Activity(string, schedulingSystem);
        Activity activity_2 = new Activity(string);
        project.addActivity(activity);
        activityHolder.setActivity(activity);

    }
    @Then("the activity exists in the project")
    public void the_activity_exists_in_the_project() {
        Project project = projectHolder.getProject();
        Activity activity = activityHolder.getActivity();

        assertTrue(project.getActivites().contains(activity
        ));

        assertEquals(schedulingSystem.getProject(activity), project);

    }

    @Then("the activity should have no project assigned to it")
    public void theActivityShouldHaveNoProjectAssignedToIt() {
        Activity activity = activityHolder.getActivity();
        assertNull(schedulingSystem.getProject(activity));
    }

    @When("the employee is assigned to the activity")
    public void theEmployeeIsAssignedToTheActivity() {
        Employee employee = employeeHolder.getEmployee();
        Activity activity = activityHolder.getActivity();
        try {
            employee.addActivity(activity);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }

    }


    @When("the employee with the id {string} is assigned to the activity")
    public void theEmployeeWithTheIdIsAssignedToTheActivity(String string) {
        Activity activity = activityHolder.getActivity();
        try {
            schedulingSystem.assignEmployeeToActivity(activity, string);
        } catch (AssertionError | Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
            errorMessageHolder.setError(e);
        }
    }

    @Then("the employee should be assigned to activity")
    public void theEmployeeShouldBeAssignedToActivity() {
        Employee employee = employeeHolder.getEmployee();
        Activity activity = activityHolder.getActivity();
        assertTrue(activity.getAssignedEmployees().contains(employee));
    }


    @When("the activity's details are changed to {string}")
    public void theActivitySDetailsAreChangedTo(String string) {
        Activity activity = activityHolder.getActivity();
        activity.setDetails(string);
    }
    @Then("the activity's details should be {string}")
    public void theActivitySDetailsShouldBe(String string) {
        Activity activity = activityHolder.getActivity();
        assertEquals(string, activity.getDetails());
    }

    @When("the employee registers {double} hours worked on the activity")
    public void theEmployeeRegistersHoursWorkedOnTheActivity(Double double1) {
        Employee employee = employeeHolder.getEmployee();
        Activity activity = activityHolder.getActivity();
        activityHolder.setAssignedHours(activity.getAssignedHours());

        LocalDate date = LocalDate.parse("2024-05-01");
        TimeRegistration newRegistration = new TimeRegistration(date, 1200, 1630, activity);
        newRegistration.setActivity(activity);

        timeRegistrationHolder.setTimeRegistration(newRegistration);

        employee.addTimeRegistration(newRegistration);
    }
    @Then("the activity should have {double} more hours worked on it")
    public void theActivityShouldHaveMoreHoursWorkedOnIt(Double double1) {
        Activity activity = activityHolder.getActivity();

        double prevHours = activityHolder.getAssignedHours();
        double newHours = activity.getWorkedHours();
        assertEquals(newHours, prevHours + double1, 0.01);
    }


    @Then("the employee should have the time registration registered to him")
    public void theEmployeeShouldHaveTheTimeRegistrationRegisteredToHim() {
        Employee employee = employeeHolder.getEmployee();
        TimeRegistration timeRegistration = timeRegistrationHolder.getTimeRegistration();
        assertTrue(employee.getTimeRegistrations().contains(timeRegistration));
    }


    @When("the activity with the name {string} is created")
    public void the_activity_with_the_name_is_created(String string) {
        Activity activity = new Activity(string);
        activity.setSchedulingSystem(schedulingSystem);
        schedulingSystem.addActivity(activity);
        activityHolder.setActivity(activity);

    }

    @Then("the activity exists in the scheduling system")
    public void the_activity_exists_in_the_scheduling_system() {
        Activity activity = activityHolder.getActivity();

        assertTrue(schedulingSystem.getActivities().contains(activity));
    }

    @Then("the activity with the name {string} exists in the scheduling system")
    public void theActivityWithTheNameExistsInTheSchedulingSystem(String string) {
        boolean sameName = false;

        for (Activity a : schedulingSystem.getActivities()) {
            if (a.getName().equals(string)) {
                sameName = true;
            }
        }

        assertTrue(activityHolder.getActivity().getSchedulingSystem().equals(schedulingSystem));

        assertTrue(sameName);
    }



    @Then("the activity should have the employee assigned to it")
    public void theActivityShouldHaveTheEmployeeAssignedToIt() {
        Activity activity = activityHolder.getActivity();
        Employee employee = employeeHolder.getEmployee();

        assertTrue(schedulingSystem.getActivities(employee).contains(activity));
        assertTrue(activity.getAssignedEmployees().contains(employee));
    }

    @Given("an employee is assigned to the activity")
    public void anEmployeeIsAssignedToTheActivity() {
        Employee employee = new Employee("huba",schedulingSystem);
        try {
            schedulingSystem.addEmployee(employee);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        Activity activity = activityHolder.getActivity();
        employeeHolder.setEmployee(employee);
        try {
            schedulingSystem.assignEmployeeToActivity(activity, employee.getId());
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }

    }



    @Then("the activity should have {double} hours assigned to it")
    public void theActivityHasHoursAssignedToIt(double arg0) {
        assertEquals(arg0, activityHolder.getActivity().getAssignedHours());
    }

    @Given("the activity is set to have {double} hours assigned to it")
    public void theActivityIsSetToHaveHoursAssignedToIt(Double double1) {
        Activity activity = activityHolder.getActivity();
        activity.setAssignedHours(double1);
    }

    @When("{double} hours is assigned to the activity")
    public void hoursIsAssignedToTheActivity(double arg0) {
        activityHolder.getActivity().setAssignedHours(arg0);
    }

    @And("the selected employee is {string}")
    public void theSelectedEmployeeIs(String arg0) {
        Employee employee = new Employee(arg0);
        try {
            schedulingSystem.setSelectedEmployee(arg0);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        employeeHolder.setEmployee(employee);
    }


    @And("the selected acitivity is {string}")
    public void theSelectedAcitivityIs(String arg0) {
        Activity activity = new Activity(arg0);
        activityHolder.setActivity(activity);
        schedulingSystem.setSelectedActivity(activity);
    }


    @Given("an activity with the name {string} exists")
    public void anActivityWithTheNameExists(String arg0) {
        Activity activity = new Activity(arg0);
        activityHolder.setActivity(activity);
        schedulingSystem.addActivity(activity);
    }

    @When("I select the activity with the name {string}")
    public void iSelectTheActivityWithTheName(String arg0) throws Exception {
        Activity activity = null;

        for (Activity a : schedulingSystem.getActivities()) {
            if (a.getName().equals(arg0)) {
                activity = a;
                break;
            }
        }
        if (activity == null) {
            errorMessageHolder.setErrorMessage("No activity with that name exists");
        } else {
            activityHolder.setActivity(activity);
            schedulingSystem.setSelectedActivity(activity);
        }
    }

    @Given("an employee exists")
    public void anEmployeeExists() {
        Employee e = new Employee("test", schedulingSystem);
        employeeHolder.setEmployee(e);
    }
    @Given("a static activity exists")
    public void aStaticActivityExists() {
        StaticActivity a = new StaticActivity("test", schedulingSystem);
        activityHolder.setActivity(a);
        activityHolder.setAssignedHours(a.getWorkedHours());
    }

    @Then("the worked hours on the activity should be unchanged")
    public void theWorkedHoursOnTheStaticActivityShouldBeUnchanged() {
        Activity activity = activityHolder.getActivity();
        assertEquals(activityHolder.getAssignedHours(), activity.getWorkedHours());
    }

    @Then("the activity's assigned hours should be unchanged")
    public void theActivitySAssignedHoursShouldBeUnchanged() {
        Activity a = activityHolder.getActivity();
        Double hours = a.getAssignedHours();
        assertEquals(a.getAssignedHours(), activityHolder.getAssignedHours());
    }

    @Given("no activity with the name {string} exists")
    public void noActivityWithTheNameExists(String string) {
        boolean sameName = false;
        for (Activity a : schedulingSystem.getActivities()) {
            if (a.getName().equals(string)) {
                sameName = true;
            }
        }
        assertFalse(sameName);
    }


    @When("the activity is selected")
    public void theActivityIsSelected() {
        Activity activity = activityHolder.getActivity();
        schedulingSystem.setSelectedActivity(activity);
    }
    @Then("the activity should be the selected activity")
    public void theActivityShouldBeTheSelectedActivity() {
        Activity activity = activityHolder.getActivity();
        assertEquals(activity, schedulingSystem.getSelectedActivity());
    }

    @When("the employee is removed from the activity")
    public void theEmployeeIsRemovedFromTheActivity() {
        Employee employee = employeeHolder.getEmployee();
        Activity activity = activityHolder.getActivity();
        try {
            schedulingSystem.removeEmployeeFromActivity(employee.getId(), activity);

        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }


    @Then("the selected activity is {string}")
    public void theSelectedActivityIs(String arg0) {
        Activity activity = activityHolder.getActivity();
        schedulingSystem.setSelectedActivity(activity);
        Activity selectedActivity = schedulingSystem.getSelectedActivity();
        assertEquals(activity, selectedActivity);

    }

    @Then("the employee should be removed from the activity")
    public void theEmployeeShouldBeRemovedFromTheActivity() {
        Activity activity = activityHolder.getActivity();
        Employee employee = employeeHolder.getEmployee();
        assertFalse(activity.getAssignedEmployees().contains(employee));
    }


}
