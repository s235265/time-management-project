package example.cucumber;

import example.cucumber.Helpers.ErrorMessageHolder;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import schedulingapp.App.SchedulingSystem;

import static org.junit.jupiter.api.Assertions.*;

public class StepDefinitions {

	private SchedulingSystem schedulingSystem;
	private ErrorMessageHolder errorMessageHolder;


	public StepDefinitions(SchedulingSystem schedulingSystem, ErrorMessageHolder errorMessageHolder) {
		this.schedulingSystem = schedulingSystem;
		this.errorMessageHolder = errorMessageHolder;
	}

	@When("I do nothing")
	public void iDoNothing() {
	}

	@Then("everything is okay")
	public void everythingIsOkay() {
		assertTrue(true);
	}

	@Then("the exception {string} is thrown")
	public void theExceptionIsThrown(String errorMessage) throws Exception {
		assertEquals(errorMessage, this.errorMessageHolder.getErrorMessage());
	}

	@Then("an AssertionError should be thrown")
	public void anAssertionErrorShouldBeThrown() {
		Throwable e = this.errorMessageHolder.getError();
        assertInstanceOf(AssertionError.class, e);
	}

	@Then("the exception null is thrown")
	public void theExceptionNullIsThrown() {
		assertNull(errorMessageHolder.getErrorMessage());
	}

}
