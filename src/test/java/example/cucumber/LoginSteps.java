package example.cucumber;

import example.cucumber.Helpers.EmployeeHolder;
import example.cucumber.Helpers.ErrorMessageHolder;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Employee;

import static org.junit.jupiter.api.Assertions.*;


public class LoginSteps {
    private ErrorMessageHolder errorMessageHolder;
    private SchedulingSystem schedulingSystem;
    private EmployeeHolder employeeHelper;

    public LoginSteps(SchedulingSystem s, ErrorMessageHolder e, EmployeeHolder h){
        this.schedulingSystem = s;
        this.errorMessageHolder = e;
        this.employeeHelper = h;
    }

    @Given("an employee is logged in")
    public void anEmployeeIsLoggedIn() {
        Employee employee = new Employee("huba");
        employeeHelper.setEmployee(employee);


        try {

            schedulingSystem.addEmployee(employee);
            schedulingSystem.logIn(employee);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        assertTrue(schedulingSystem.getLoggedInUser().equals(employee));
    }


    @Given("an employee exists with the id {string}")
    public void anEmployeeExistsWithTheId(String arg0) {

        Employee employee = new Employee(arg0);
        try {
            schedulingSystem.addEmployee(employee);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        employeeHelper.setAssignedHours(employee.getAssignedHours());
        employeeHelper.setEmployee(employee);
        assertTrue(schedulingSystem.hasEmployee(arg0));
    }

    @When("the user logs in with the id {string}")
    public void the_user_logs_in_with_the_id(String string) {
        Employee employee = new Employee(string);
        try {
            schedulingSystem.logIn(employee);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }
    @Then("the employee with id {string} is logged in")
    public void the_employee_with_id_is_logged_in(String string) {
        assertTrue(schedulingSystem.getLoggedInUser().getId().equals(string));
    }
    @Given("there is no employee with the id {string}")
    public void there_is_no_employee_with_the_id(String string) {
        Employee employee = new Employee(string, schedulingSystem);
        employeeHelper.setEmployee(employee);
        assertFalse(schedulingSystem.hasEmployee(string));
    }
    @Then("the exception {string} is given\"")
    public void the_exception_is_given(String string) throws Exception {
        assertEquals(string, this.errorMessageHolder.getErrorMessage());
    }

    @When("the user logs out")
    public void theUserLogsOut() {
        schedulingSystem.logOut();

    }

    @Then("the user is logged out")
    public void theUserIsLoggedOut() {
        assertEquals(null, schedulingSystem.getLoggedInUser());
    }
}
