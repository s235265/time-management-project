package example.cucumber;

import example.cucumber.Helpers.ActivityHolder;
import example.cucumber.Helpers.EmployeeHolder;
import example.cucumber.Helpers.ErrorMessageHolder;
import example.cucumber.Helpers.ProjectHolder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_old.Ac;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Employee;
import schedulingapp.Domain.Project;

import static org.junit.jupiter.api.Assertions.*;



public class ProjectSteps {

    private SchedulingSystem schedulingSystem;
    private ErrorMessageHolder errorMessageHolder;
    private ProjectHolder projectHolder;
    private ActivityHolder activityHolder;
    private EmployeeHolder employeeHolder;

    public ProjectSteps(SchedulingSystem s, ErrorMessageHolder errorMessageHolder, ProjectHolder projectHolder, ActivityHolder a, EmployeeHolder e) {
        this.schedulingSystem = s;
        this.errorMessageHolder = errorMessageHolder;
        this.projectHolder = projectHolder;
        this.activityHolder = a;
        this.employeeHolder = e;
    }


    @Then("the project with the name {string} should exist")
    public void aProjectWithTheNameStringShouldExist(String string) {
        Project project = projectHolder.getProject();

        boolean sameName = false;
        for (Project p : schedulingSystem.getProjects()) {
            if (p.getName().equals(string)) {
                sameName = true;
            }
        }

        assertEquals(schedulingSystem, project.getSchedulingSystem());
        assertTrue(sameName);
    }

    @Given("a project exists")
    public void aProjectExists() {
        Project p = new Project("TestingProject", schedulingSystem);
        projectHolder.setProject(p);
    }
    @When("the activity is removed from the project")
    public void theActivityIsRemovedFromTheProject() {
        Activity a = activityHolder.getActivity();
        Project p = projectHolder.getProject();

        p.removeActivity(a);
    }
    @Then("the activity should not be in the project")
    public void theActivityShouldNotBeInTheProject() {
        Activity a = activityHolder.getActivity();
        Project p = projectHolder.getProject();
        assertFalse(p.getActivites().contains(a));
    }

    @When("the project leader of the project is set to the employee")
    public void theProjectLeaderOfTheProjectIsSetToTheEmployee() {
        Project p = projectHolder.getProject();
        Employee e = employeeHolder.getEmployee();

        try {
            p.setProjectLeader(e);
        } catch (Exception ex) {
            errorMessageHolder.setErrorMessage(ex.getMessage());
        }
    }
    @Then("the project leader of the project should be {string}")
    public void theProjectLeaderOfTheProjectShouldBe(String string) {
        Project p = projectHolder.getProject();

        assertEquals(string, p.getProjectLeader().getId());
    }






    @Given("there is no project with the name {string}")
    public void thereIsNoProjectWithTheName(String string) {

        Project p = new Project(string, schedulingSystem);
        projectHolder.setProject(p);

        boolean sameName = false;
        for (Project project : schedulingSystem.getProjects()) {
            if (project.getName().equals(string)) {
                sameName = true;
            }
        }
        assertFalse(sameName);
    }

    @Given("a project with the name {string} exists")
    public void aProjectWithTheNameExists(String string) {
        Project p = new Project(string, schedulingSystem);
        try {
            schedulingSystem.addProject(p);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        projectHolder.setProject(p);
    }

    @When("the project is removed from the system")
    public void theProjectIsRemovedFromTheSystem() {
        Project p = projectHolder.getProject();
        try {
            schedulingSystem.removeProject(p);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }
    @Then("the project should be removed from the system")
    public void theProjectShouldBeRemovedFromTheSystem() {
        Project p = projectHolder.getProject();
        assertFalse(schedulingSystem.hasProject(p));
    }

    @And("a project with the name {string} is created")
    public void aProjectWithTheNameIsCreated(String string) {
        Project project = new Project(string, schedulingSystem);
        try {
            schedulingSystem.addProject(project);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }

    }

    @And("there is a project with the name {string}")
    public void thereIsAProjectWithTheName(String arg0) throws Exception {
        Project project = new Project(arg0);
        projectHolder.setProject(project);
        schedulingSystem.addProject(project);

    }

    @Given("the activity is assigned to the project")
    public void theActivityIsAssignedToTheProject() {
        Activity activity = activityHolder.getActivity();
        Project project = projectHolder.getProject();
        project.addActivity(activity);
    }
    @Then("the employee should be assigned to the project")
    public void theEmployeeShouldBeAssignedToTheProject() {
        Employee employee = employeeHolder.getEmployee();
        Project project = projectHolder.getProject();

        assertTrue(employee.getProjects().contains(project));
    }

    @When("I select the project with the name {string}")
    public void iSelectTheProjectWithTheName(String arg0) {
        Project project = schedulingSystem.getSelectedProject();
        try {
            project = schedulingSystem.findProject(arg0);
        } catch (Exception e) {
            e = new Exception("No project with that name exists");
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        projectHolder.setProject(project);
        schedulingSystem.setSelectedProject(project);
    }

    @Then("the selected project is {string}")
    public void theSelectedProjectIs(String arg0) {
        Project project = projectHolder.getProject();
        Project selectedProject = schedulingSystem.getSelectedProject();
        assertEquals(project, selectedProject);
    }

}

