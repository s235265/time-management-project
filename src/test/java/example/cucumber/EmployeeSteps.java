package example.cucumber;

import example.cucumber.Helpers.ActivityHolder;
import example.cucumber.Helpers.EmployeeHolder;
import example.cucumber.Helpers.ErrorMessageHolder;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_old.Ac;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Employee;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeSteps {

    private EmployeeHolder employeeHolder;
    private SchedulingSystem schedulingSystem;
    private ErrorMessageHolder errorMessageHolder;
    private ActivityHolder activityHolder;

    public EmployeeSteps(EmployeeHolder e, SchedulingSystem s, ErrorMessageHolder errorMessageHolder, ActivityHolder activityHolder) {
        this.employeeHolder = e;
        this.schedulingSystem = s;
        this.errorMessageHolder = errorMessageHolder;
        this.activityHolder = activityHolder;

    }

    @When("an employee is created with the id {string}")
    public void an_employee_is_created_with_the_id(String string) {
        Employee employee = new Employee(string);
        employeeHolder.setEmployee(employee);
        try {
            schedulingSystem.addEmployee(employee);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }

    }

    @Then("the employee's assigned hours should increment with {double}")
    public void theEmployeeSAssignedHoursShouldIncrementWithTheAmountOfHoursOfTheActivity(double hours) {

        double old_hours = employeeHolder.getAssignedHours();
        double new_hours = employeeHolder.getEmployee().getAssignedHours();

        assertEquals(old_hours + hours, new_hours);
    }

    @Then("the employee exists in the system")
    public void the_employee_exists_in_the_system() {
        Employee employee = employeeHolder.getEmployee();

        assertTrue(schedulingSystem.getEmployees().contains(employee));
    }

    @When("I select the employee with the id {string}")
    public void iSelectTheEmployeeWithTheId(String string) {
        Employee employee = null;
        try {
            employee = schedulingSystem.findEmployee(string);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
        employeeHolder.setEmployee(employee);
        try {
            schedulingSystem.setSelectedEmployee(string);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }

    @Given("an employee doesn't exist with the id {string}")
    public void anEmployeeDoesnTExistsWithTheId(String string) {
        Employee employee = new Employee(string, schedulingSystem);
        employeeHolder.setEmployee(employee);
        assertFalse(schedulingSystem.getEmployees().contains(employee));
    }

    @Given("an employee exists and is null")
    public void anEmployeeExistsAndIsNull() {
        employeeHolder.setEmployee(null);
        assertNull(employeeHolder.getEmployee());
    }

    @Then("the selected employee is set to {string}")
    public void theSelectedEmployeeIsSetTo(String arg0) {
        Employee employee = employeeHolder.getEmployee();
        Employee selectedEmployee = schedulingSystem.getSelectedEmployee();
        assertEquals(employee, selectedEmployee);

    }


}
