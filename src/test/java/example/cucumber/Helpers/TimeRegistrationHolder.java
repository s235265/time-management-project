package example.cucumber.Helpers;

import schedulingapp.Domain.Activity;
import schedulingapp.Domain.TimeRegistration;

public class TimeRegistrationHolder {

    private TimeRegistration timeRegistration;

    public TimeRegistration getTimeRegistration() {
        return timeRegistration;
    }

    public void setTimeRegistration(TimeRegistration timeRegistration) {
        this.timeRegistration = timeRegistration;
    }
}




