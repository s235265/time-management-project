package example.cucumber.Helpers;

import schedulingapp.Domain.Project;

public class ProjectHolder {

    private Project project;

    public ProjectHolder(){
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
