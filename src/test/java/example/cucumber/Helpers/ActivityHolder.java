package example.cucumber.Helpers;

import schedulingapp.Domain.Activity;

public class ActivityHolder {

    private Activity activity;
    private Double assignedHours;

    public ActivityHolder(){

    }

    public void setAssignedHours(Double assignedHours) {
        this.assignedHours = assignedHours;
    }
    public Double getAssignedHours() {
        return assignedHours;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}



