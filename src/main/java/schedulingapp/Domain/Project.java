package schedulingapp.Domain;

import schedulingapp.App.SchedulingSystem;

import java.util.ArrayList;

public class Project {
    private String name;
    private ArrayList<Activity> activities = new ArrayList<>();
    private SchedulingSystem schedulingSystem;
    private Employee projectLeader = null;

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, SchedulingSystem schedulingSystem) {
        this.name = name;
        this.schedulingSystem = schedulingSystem;
    }

    public String getName() {
        return name;
    }

    public void addActivity(Activity activity) {
        activities.add(activity);
    }

    public SchedulingSystem getSchedulingSystem() {
        return schedulingSystem;
    }

    public void setSchedulingSystem(SchedulingSystem schedulingSystem) {
        this.schedulingSystem = schedulingSystem;
    }

    public ArrayList<Activity> getActivites() {
        return activities;
    }

    public void setProjectLeader(Employee projectLeader) throws Exception {
        this.projectLeader = schedulingSystem.findEmployee(projectLeader.getId());
    }

    public Employee getProjectLeader() {
        return projectLeader;
    }

    public void removeActivity(Activity activity) {
        activity.getAssignedEmployees().clear();
        activities.remove(activity);
        getSchedulingSystem().setSelectedActivity(null);
    }
}
