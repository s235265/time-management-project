package schedulingapp.Domain;

import schedulingapp.App.SchedulingSystem;

import java.util.ArrayList;
import java.util.List;

public class Activity {
    private double assignedHours = 0;
    private String name = "";
    private List<Employee> assignedEmployees = new ArrayList<Employee>();
    private SchedulingSystem schedulingSystem;
    private String details = "";
    private double workedHours = 0;

    public Activity(String name, SchedulingSystem schedulingSystem) {
        this.name = name;
        this.schedulingSystem = schedulingSystem;
    }

    public Activity(String name) {
        this.name = name;
    }

    public void addAssignedEmployee(Employee employee) {
        assignedEmployees.add(employee);
    }

    public double getAssignedHours() {
        return assignedHours;
    }

    public void setAssignedHours(double assignedHours) {
        this.assignedHours = assignedHours;
    }

    public String getName() {
        return name;
    }

    public List<Employee> getAssignedEmployees(){
        return assignedEmployees;
    }


    public SchedulingSystem getSchedulingSystem() {
        return schedulingSystem;
    }

    public void setSchedulingSystem(SchedulingSystem schedulingSystem) {
        this.schedulingSystem = schedulingSystem;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void removeEmployee(Employee employee) {
        if (assignedEmployees.contains(employee)) {
            assignedEmployees.remove(employee);
        }
    }

    public void addWorkedHours(double hoursWorked) {
        workedHours += hoursWorked;
    }

    public double getWorkedHours(){
        return workedHours;
    }
}
