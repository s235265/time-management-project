package schedulingapp.Domain;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Displays.MenuScreen;

import java.util.Scanner;

public abstract class Display {

    private static SchedulingSystem schedulingSystem;
    private static Display nextDisplay = new MenuScreen(schedulingSystem);


    public Display(SchedulingSystem s) {
        schedulingSystem = s;
    }

    protected SchedulingSystem getSchedulingSystem() {
        return schedulingSystem;
    }

    public Display getNextDisplay(){
        return nextDisplay;
    }

    public static void clearScreen(){
        emptyLines(200);
    }


    public static void emptyLines(int n){
        for (int i = 0; i < n; i++){
            System.out.println("");
        }
    }


    protected void setNextDisplay(Display display){
        nextDisplay = display;
    }

    public abstract void showScreen();

    public abstract Object getInput(Scanner console) throws Exception;

    public abstract void actOnInput(Object input) throws Exception;
}
