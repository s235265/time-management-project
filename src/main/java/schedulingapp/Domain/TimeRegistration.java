package schedulingapp.Domain;

import java.time.LocalDate;
import java.util.Scanner;

import static java.lang.Math.floor;

public class TimeRegistration {
    private LocalDate date;
    private int startTime;
    private int endTime;
    private Activity activity;

    public TimeRegistration(LocalDate date, int startTime, int endTime, Activity activity) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        activity.addWorkedHours(getHoursWorked());
    }

    public LocalDate getDate() {
        return date;
    }

    public double getHoursWorked(){

        int startHour = (int) floor(startTime*0.01);
        int endHour = (int) floor(endTime*0.01);

        int startMin = startTime % 100;
        int endMin = endTime % 100;

        Double hoursWorked = (double) endHour - startHour;

        if (endMin > startMin){
            hoursWorked += 0.5;
        } else if (endMin < startMin){
            hoursWorked -= 0.5;
        }
        return hoursWorked;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

}
