package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.StaticActivity;
import schedulingapp.Domain.TimeRegistration;

import java.util.Scanner;

public class StaticActivityScreen extends ActivityScreen {
    private StaticActivity staticActivity;
    private Scanner console;

    public StaticActivityScreen(SchedulingSystem s) {
        super(s);
    }

    public void setActivity(StaticActivity a) {
        this.staticActivity = a;
    }

    @Override
    public void showScreen() {
        clearScreen();
        System.out.println("System Activity: " + staticActivity.getName());
        System.out.println();
        System.out.println();
        System.out.println("--------------------------");
        System.out.println("Enter 0 to exit, or 1 to assign hours to this activity");
        System.out.println("--------------------------------------------------------------------------");
    }

    @Override
    public Object getInput(Scanner console) {
        this.console = console;
        return console.next();
    }

    @Override
    public void actOnInput(Object input) throws Exception {
        try {
            int inputNum = Integer.parseInt(input.toString());
            switch (inputNum) {
                case 0:
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                case 1:
                    TimeRegistration registration = getTimeRegistration(console);
                    getSchedulingSystem().getLoggedInUser().addTimeRegistration(registration);
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                default:
                    setNextDisplay(this);
                    break;
            }
        } catch (NumberFormatException e) {
            setNextDisplay(this);
        }

    }



    public void setStaticActivity(StaticActivity staticActivity) {
        this.staticActivity = staticActivity;
    }

    public StaticActivity getStaticActivity() {
        return staticActivity;
    }


}
