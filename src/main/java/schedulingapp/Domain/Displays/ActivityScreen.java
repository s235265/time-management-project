package schedulingapp.Domain.Displays;
import io.cucumber.java.en_old.Ac;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.*;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Scanner;

public class ActivityScreen extends Display {
    private Activity activity = getSchedulingSystem().getSelectedActivity();
    private Scanner console;

    public ActivityScreen(SchedulingSystem s) {
        super(s);
    }

    @Override
    public void showScreen(){
        clearScreen();
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Activity: " + activity.getName());
        System.out.println("\tDetails: " + activity.getDetails());
        System.out.println("\tAssigned Hours: " + activity.getAssignedHours());
        System.out.println();
        System.out.println("\tTotal Worked Hours: " + activity.getWorkedHours());
        System.out.println();
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Assigned Employees: ");

        for (int i = 0; i < activity.getAssignedEmployees().size(); i++) {
            Employee e = activity.getAssignedEmployees().get(i);
            System.out.println((i+2) + ") " + e.getId());
        }
        System.out.println();
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Enter employee_id to assign them to this activity, or -employee_id to remove them");
        System.out.println("For example enter \"huba\" to assign huba");
        System.out.println("or \"-huba\" to remove huba");
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Enter 1 to assign hours to this activity");
        System.out.println("0 to exit");
        System.out.println("-1 to change details");
        System.out.println("-2 to change assigned hours");
        System.out.println("-3 to delete the activity");
        System.out.println("--------------------------------------------------------------------------");
    }

    public boolean invalid(Object input) throws Exception{
        try {
            int InputNum = Integer.parseInt(input.toString());
            if (InputNum < -3 || InputNum > activity.getAssignedEmployees().size()+1) {
                return true;
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            String inputString = input.toString();

            if (getSchedulingSystem().hasEmployee(inputString)){
                return false;
            }
            if (inputString.charAt(0) == '-' && getSchedulingSystem().hasEmployee(inputString.substring(1))) {
                Employee employee = getSchedulingSystem().findEmployee(inputString.substring(1));
                if (activity.getAssignedEmployees().contains(employee)){
                    return false;
                }
            }

            return true;
        }
    }

    @Override
    public Object getInput(Scanner console) throws Exception {
        this.console = console;
        Object input = console.next();

        while (invalid(input)){
            showScreen();
            System.out.println("Invalid option");
            input = console.next();
        }
        return input;
    }

    public boolean notDouble(String input){
        try {
            double InputNum = Double.parseDouble(input.toString());
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public void actOnInput(Object input) throws Exception {
        try {
            int inputNum = Integer.parseInt(input.toString());

            switch (inputNum){
                case -3:
                    Project p = getSchedulingSystem().getProject(activity);
                    p.removeActivity(activity);

                    getSchedulingSystem().setSelectedProject(p);
                    setNextDisplay(new ProjectScreen(getSchedulingSystem()));
                    break;
                case -2:
                    System.out.println("Enter new assigned hours");
                    String inputString = console.next();
                    while (notDouble(inputString)){
                        System.out.println("Invalid, please enter assigned hours");
                        inputString = console.next();
                    }
                    activity.setAssignedHours(Double.parseDouble(inputString));
                    setNextDisplay(this);
                    break;

                case -1:
                    String inputDetails = console.nextLine();
                    while (inputDetails.isEmpty()){
                        System.out.println("Enter details");
                        inputDetails = console.nextLine();
                    }
                    activity.setDetails(inputDetails);
                    setNextDisplay(this);
                    break;
                case 0:
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                case 1:
                    TimeRegistration registration = getTimeRegistration(console);
                    getSchedulingSystem().getLoggedInUser().addTimeRegistration(registration);
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                default:
                    getSchedulingSystem().setSelectedEmployee(activity.getAssignedEmployees().get(inputNum - 2));
                    setNextDisplay(new EmployeeInfoScreen(getSchedulingSystem()));
                    break;
            }
        } catch (NumberFormatException e) {
            String inputString = input.toString();

            if (inputString.charAt(0) == '-') {
                getSchedulingSystem().removeEmployeeFromActivity(inputString.substring(1),activity);
            } else {
                try {
                    getSchedulingSystem().assignEmployeeToActivity(activity, inputString);
                } catch (Exception ex) {
                    System.out.println("Employee already assigned to that activity");
                }
            }

            setNextDisplay(this);
        }

    }

    protected TimeRegistration getTimeRegistration(Scanner console) {
        int startTime = getValidStartTime(console);
        int endTime = getValidEndTime(console, startTime);
        return new TimeRegistration(getValidDate(console), startTime, endTime, activity);
    }

    protected int getValidEndTime(Scanner console, int startTime) {
        System.out.println("Please enter a valid end time to the nearest half hour (12:30 would be \"1230\")");
        String input = console.nextLine();
        while (invalidTime(input) || Integer.parseInt(input) < startTime){
            System.out.println("Please enter a valid end time (12:30 would be \"1230\")");
            input = console.nextLine();
        }
        return Integer.parseInt(input);
    }

    protected int getValidStartTime(Scanner console) {
        System.out.println("Please enter a valid start time to the nearest half hour (12:30 would be \"1230\")");
        String input = console.nextLine();
        while (invalidTime(input)){
            System.out.println("Please enter a valid start time (12:30 would be \"1230\")");
            input = console.nextLine();
        }
        return Integer.parseInt(input);
    }



    protected boolean invalidTime(String input) {
        if (input.length() != 4) {
            return true;
        } else {
            try {
                int startHour = Integer.parseInt(input.substring(0, 2));
                int startMin = Integer.parseInt(input.substring(2, 4));

                if (startHour >= 24 || startHour < 0 || (startMin != 30 && startMin != 0)) {
                    return true;
                }

                return false;

            } catch (NumberFormatException e) {
                return true;
            }
        }
    }

    protected LocalDate getValidDate(Scanner console) {
        System.out.println("Please enter a date YYYY-MM-DD: ");
        while (true) {
            try {
                String input = console.next();
                return LocalDate.parse(input);
            } catch (Exception e) {
                System.out.println("Please enter a valid date YYYY-MM-DD: ");
            }
        }
    }
}
