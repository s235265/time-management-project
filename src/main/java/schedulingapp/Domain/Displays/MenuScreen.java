package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Display;

import java.util.Scanner;

public class MenuScreen extends Display {

    public MenuScreen(SchedulingSystem schedulingSystem) {
        super(schedulingSystem);
    }

    @Override
    public void showScreen() {
        clearScreen();
        System.out.println("Welcome \""+ getSchedulingSystem().getLoggedInUser().getId() +"\". Here are your options:");
        System.out.println("--------------------------------------------------------------------------------------");
        System.out.println("0. Exit     1. Employee Info     2. Projects     3. Employees     4. View Worked Hours");
        System.out.println("--------------------------------------------------------------------------------------");

    }

    @Override
    public Object getInput(Scanner console) {
        Object input = console.next();
        while (invalid(input)){
            showScreen();
            System.out.println("Invalid option");
            input = console.next();
        }

        return input;
    }

    private boolean invalid(Object input) {

        try {
            int inputNum = Integer.parseInt(input.toString());
            if (inputNum < 0 || inputNum > 4) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return true;
        }


    }

    @Override
    public void actOnInput(Object input) {

        switch (Integer.parseInt(input.toString())){
            case 0:
                setNextDisplay(new LogInScreen(getSchedulingSystem()));
                break;
            case 1:
                getSchedulingSystem().setSelectedEmployee(getSchedulingSystem().getLoggedInUser());
                setNextDisplay(new EmployeeInfoScreen(getSchedulingSystem()));
                break;
            case 2:
                setNextDisplay(new ProjectsListScreen(getSchedulingSystem()));
                break;
            case 3:
                setNextDisplay(new EmployeesListScreen(getSchedulingSystem()));
                break;
            case 4:
                setNextDisplay(new HoursWorkedScreen(getSchedulingSystem()));
                break;
            default:
                throw new IllegalArgumentException("Invalid option");
        }


        }
    }

