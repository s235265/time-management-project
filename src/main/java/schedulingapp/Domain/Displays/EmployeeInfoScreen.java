package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class EmployeeInfoScreen extends Display{

    private Employee employee = getSchedulingSystem().getSelectedEmployee();
    private ArrayList<Activity> activities = new ArrayList<>();
    private int index;

    public EmployeeInfoScreen(SchedulingSystem s) {
        super(s);
    }


    @Override
    public void showScreen() {
        clearScreen();

        employee = getSchedulingSystem().getSelectedEmployee();

        System.out.println("Selected employee: " + employee.getId());
        System.out.println("Total assigned hours: " + employee.getAssignedHours());
        System.out.println("--------------------------------------------------------------------------");

        System.out.println("System activites");

        index = 1;

        for (Activity a : getSchedulingSystem().getActivities()) {
            activities.add(a);
            System.out.println("\t" + index + ") " + a.getName());
            index++;
        }

        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Project Activities:");
        System.out.println();
        
        for (Project project: employee.getProjects()) {
            if (project != null) {
                System.out.println("Project name: " + project.getName());
                for (Activity activity : project.getActivites()){
                    if (activity.getAssignedEmployees().contains(employee)){
                        System.out.println("\t" + index + ") Activity: " + activity.getName() + " | Assigned hours: " + activity.getAssignedHours());
                        activities.add(activity);
                        index++;
                    }
                }
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("0 to go to the menu screen,");
        System.out.println("or select an activity with the corresponding integer");
        System.out.println("--------------------------------------------------------------------------");
    }

    @Override
    public Object getInput(Scanner console) {

        String input = console.next();

        while (invalid(input)){
            showScreen();
            System.out.println("Invalid input");
            input = console.next();
        }

        return input;
    }

    private boolean invalid(String input){
        try {
            int inputNum = Integer.parseInt(input);

            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public void actOnInput(Object input) {

        try {
            int inputNum = Integer.parseInt(input.toString());

            switch (inputNum) {
                case 0:
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                default:
                    if (inputNum > index-1 || inputNum < 0){
                        setNextDisplay(this);
                    } else {
                        Activity chosenActivity = activities.get(inputNum-1);
                        getSchedulingSystem().setSelectedActivity(chosenActivity);

                        if (chosenActivity instanceof StaticActivity){
                            StaticActivityScreen staticActivityScreen = new StaticActivityScreen(getSchedulingSystem());
                            staticActivityScreen.setStaticActivity((StaticActivity) chosenActivity);

                            setNextDisplay(staticActivityScreen);
                        } else {
                            setNextDisplay(new ActivityScreen(getSchedulingSystem()));
                        }
                    }
                    break;
            }
        } catch (NumberFormatException e) {
            // input is string
            setNextDisplay(this);
        }


    }
}
