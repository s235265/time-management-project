package schedulingapp.Domain.Displays;

import schedulingapp.App.App;
import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.Employee;

import java.awt.*;
import java.util.Scanner;

public class LogInScreen extends Display {

    public LogInScreen(SchedulingSystem schedulingSystem) {
        super(schedulingSystem);
    }

    public void showScreen() {
        clearScreen();
        emptyLines(3);
        System.out.println("-----------------------------------------------------------");
        System.out.println("| #  #  #   #####   #       #####   #####   #   #   ##### |");
        System.out.println("| #  #  #   #       #       #       #   #   ## ##   #     | ");
        System.out.println("| ## # ##   #####   #       #       #   #   # # #   ##### | ");
        System.out.println("|  #   #    #       #       #       #   #   #   #   #     |");
        System.out.println("|  #   #    #####   #####   #####   #####   #   #   ##### |");
        System.out.println("-----------------------------------------------------------");
        System.out.println("enter login credentials:");
    }

    public Object getInput(Scanner console){
        String input = console.next();
        while (invalid(input)){
            showScreen();
            System.out.println("Please enter a valid input..");
            input = console.next();
        }


        return input;
    }

    @Override
    public void actOnInput(Object input) {
        setNextDisplay(new MenuScreen(getSchedulingSystem()));

        try {
            getSchedulingSystem().logIn(new Employee((String) input));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean invalid(String input){
        try {
            int inputNum = Integer.parseInt(input);
            if (inputNum != 0){
                return true;
            }
        } catch (Exception e) {
            if (getSchedulingSystem().hasEmployee(input)){
                return false;
            }
        }
        return true;
    }

    public boolean hasNumbers(String input){
        for (int i = 0; i < input.length(); i++){
            if (Character.isDigit(input.charAt(i))){
                return true;
            }
        }
        return false;
    }


}
