package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.Employee;
import schedulingapp.Domain.Project;

import java.util.ArrayList;
import java.util.Scanner;

public class ProjectsListScreen extends Display{

    private ArrayList<Project> allProjects = getSchedulingSystem().getProjects();

    public ProjectsListScreen(SchedulingSystem s) {
        super(s);
    }

    @Override
    public void showScreen() {
        String currentProject;
        clearScreen();
        int i = 0;

        for (Project project : allProjects) {
            System.out.println();
            if (project != null) {
                System.out.println((i+2)+") Project name: \"" + project.getName() + "\"");
                i++;
                for (Activity activity : project.getActivites()){
                        System.out.println("- Activity: " + activity.getName());
                }
            }

        }
        if(getSchedulingSystem().getSelectedProject() == null){
            currentProject = "No project selected";
        } else {
            currentProject = getSchedulingSystem().getSelectedProject().getName();
        }
        System.out.println();
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Here are all the projects. Press -1 to enter selected project\n0 to go back\nor press 1 to create project");
        System.out.println("Enter an integer to select a project");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Selected Project: " + currentProject);
        System.out.println("--------------------------------------------------------------------------");

    }

    @Override
    public Object getInput(Scanner console) {

        String input = console.next();
        while (invalid(input)){
            showScreen();
            System.out.println("Invalid option");
            input = console.next();
        }

        return input;
    }

    private boolean invalid(String input) {
        for (Project project : allProjects) {
            if (project.getName().equals(input)) {
                return false;
            }
        }


        try {
            int inputNum = Integer.parseInt(input);

            if (inputNum < -1 || inputNum > allProjects.size() + 1) {
                return true;
            }

        } catch (NumberFormatException e) {
            return true;
        }
        return false;

    }

    @Override
    public void actOnInput(Object input) throws Exception {

        try {
            int inputNum = Integer.parseInt(input.toString());

        switch (Integer.parseInt(input.toString())){
            case -1:
                if (getSchedulingSystem().getSelectedProject() == null){
                    System.out.println("No project selected");
                    setNextDisplay(this);
                } else {
                    setNextDisplay(new ProjectScreen(getSchedulingSystem()));
                }
                break;

            case 0:
                setNextDisplay(new MenuScreen(getSchedulingSystem()));
                break;

            case 1:
                Scanner s = new Scanner(System.in);
                System.out.print("Enter project name: ");
                Project p = new Project(s.nextLine());
                if(getSchedulingSystem().hasProject(p)) {
                    showScreen();
                    System.out.println("Project already exists");
                }
                else{
                    getSchedulingSystem().addProject(p);
                }
                break;

                default:
                    getSchedulingSystem().setSelectedProject(allProjects.get(inputNum-2));
                    setNextDisplay(this);
            }
        } catch (NumberFormatException e) {
            getSchedulingSystem().setSelectedEmployee((String) input);
            setNextDisplay(this);
        }
    }
}
