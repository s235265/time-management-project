package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.Employee;

import java.awt.*;
import java.util.*;

public class EmployeesListScreen extends Display{
    private boolean sortedAlphabetically = false;
    private ArrayList<Employee> employees = getSchedulingSystem().getEmployees();

    public EmployeesListScreen(SchedulingSystem s) {
        super(s);
    }

    @Override
    public void showScreen() {
        clearScreen();

        if (sortedAlphabetically) {
            Collections.sort(employees, new Comparator<Employee>() {
                @Override
                public int compare(Employee a, Employee b) {
                    return a.getId().compareTo(b.getId());
                }
            });
        } else {
                Collections.sort(employees, new Comparator<Employee>() {
                    @Override
                    public int compare(Employee a, Employee b) {
                        if (a.getAssignedHours() == b.getAssignedHours()) {
                            return 0;
                        }
                        return a.getAssignedHours() < b.getAssignedHours() ? -1 : 1;
                    }
                });

            }



        System.out.println("--------------------------------------------------------------------------");

        for (int i = 0; i < employees.size(); i++) {
            System.out.println((i+2) + ") ID: " + employees.get(i).getId());
            System.out.println("\t Assigned Hours: " + employees.get(i).getAssignedHours());
        }


        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Please select an employee by typing the corresponding integer or their id");
        System.out.println("Selected Employee: " + getSchedulingSystem().getSelectedEmployee().getId());
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("-1 to see employee info");
        System.out.println("0 to exit");
        System.out.println("1 to rearrange sorting");
        System.out.println("--------------------------------------------------------------------------");
    }

    @Override
    public Object getInput(Scanner console) {

        String input = console.next();
        while (invalid(input)){
            showScreen();
            System.out.println("Invalid option");
            input = console.next();
        }

        return input;
    }

    private boolean invalid(String input) {
        for (Employee employee : employees) {
            if (employee.getId().equals(input)) {
                return false;
            }
        }


        try {
            int inputNum = Integer.parseInt(input);

            if (inputNum < -1 || inputNum > employees.size() + 1) {
                return true;
            }

        } catch (NumberFormatException e) {
            return true;
        }
        return false;

    }

    @Override
    public void actOnInput(Object input) {
        try {
            int inputNum = Integer.parseInt(input.toString());

            switch (inputNum){
                case -1:
                    setNextDisplay(new EmployeeInfoScreen(getSchedulingSystem()));
                    break;
                case 0:
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                case 1:
                    setNextDisplay(this);
                    sortedAlphabetically = !sortedAlphabetically;
                    break;
                default:
                    getSchedulingSystem().setSelectedEmployee(employees.get(inputNum-2));
                    setNextDisplay(this);
                    break;
            }


        } catch (NumberFormatException e) {
            try {
                getSchedulingSystem().setSelectedEmployee((String) input);
            } catch (Exception ex) {
                System.out.println("No employee with that id exists");
            }
            setNextDisplay(this);
        }
    }
}
