package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.Employee;
import schedulingapp.Domain.Project;

import java.util.Scanner;

public class ProjectScreen extends Display {

    private Project project = getSchedulingSystem().getSelectedProject();


    public ProjectScreen(SchedulingSystem s) {
        super(s);
    }


    @Override
    public void showScreen() {
        clearScreen();

        project = getSchedulingSystem().getSelectedProject();

        System.out.println("Selected project: " + project.getName());
       // System.out.println("Total hours: " + project.getTotalHours());
        System.out.println("--------------------------------------------------------------------------");


        for (int i = 0; i < project.getActivites().size(); i++) {
            if (project != null) {
                Activity activity = project.getActivites().get(i);
                System.out.println((i+2) + ") - Activity: " + activity.getName()+ " | Hours: " + activity.getAssignedHours());
            }
        }
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Selected project: " + project.getName());
        if (project.getProjectLeader() != null) {
            System.out.println("Project leader: " + project.getProjectLeader().getId());
        }
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("Enter integer to view an activity");
        System.out.println("-1 to delete the project");
        System.out.println("0 to exit");
        System.out.println("1 to create a new activity");
        System.out.println("Enter an id to assign them as project leader");
        System.out.println("--------------------------------------------------------------------------");
    }

    @Override
    public Object getInput(Scanner console) {

        Object input = console.next();

        while (invalid(input)){
            showScreen();
            System.out.println("Invalid input");
            input = console.next();
        }
        return input;
    }

    private boolean invalid(Object input) {
        try {
            int inputNum = Integer.parseInt(input.toString());
            if (inputNum < -1 || inputNum > project.getActivites().size()+1) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            if (getSchedulingSystem().hasEmployee(input.toString())){
                return false;
            }
            return true;
        }
    }

    @Override
    public void actOnInput(Object input) {

        try {
            int inputNum = Integer.parseInt(input.toString());

            switch (inputNum) {
                case -1:

                    try {
                        getSchedulingSystem().removeProject(project);
                    } catch (Exception e) {
                        System.out.println("No such project exists");
                    }

                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;
                case 0:
                    setNextDisplay(new MenuScreen(getSchedulingSystem()));
                    break;

                case 1:
                    Scanner s = new Scanner(System.in);
                    System.out.print("Enter activity name: ");
                    Activity a = new Activity(s.nextLine());
                    if(getSchedulingSystem().getSelectedProject().getActivites().contains(a)) {
                        showScreen();
                        System.out.println("Activity already exists");
                    }
                    else{
                        getSchedulingSystem().getSelectedProject().addActivity(a);
                    }
                    System.out.println("Set assigned hours:");
                    while (!s.hasNextDouble()) {
                        System.out.println("Invalid input. Please enter a number.");
                        s.next();
                    }
                    a.setAssignedHours(s.nextDouble());
                    break;

                default:
                    getSchedulingSystem().setSelectedActivity(project.getActivites().get(inputNum-2));
                    setNextDisplay(new ActivityScreen(getSchedulingSystem()));
                    break;
            }
        } catch (NumberFormatException e) {
            try {
                Employee projectLeader = getSchedulingSystem().findEmployee(input.toString());
                project.setProjectLeader(projectLeader);
            } catch (Exception ex) {
                System.out.println("Can't find that id");
            }
            setNextDisplay(this);
        }


    }
}

