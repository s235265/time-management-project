package schedulingapp.Domain.Displays;

import schedulingapp.App.SchedulingSystem;
import schedulingapp.Domain.Display;
import schedulingapp.Domain.Employee;
import schedulingapp.Domain.TimeRegistration;

import java.util.Scanner;

public class HoursWorkedScreen extends Display {

    public HoursWorkedScreen(SchedulingSystem s){
        super(s);
    }


    @Override
    public void showScreen() {
        Employee employee = getSchedulingSystem().getLoggedInUser();

        clearScreen();

        for (TimeRegistration registration : employee.getTimeRegistrations()) {
            System.out.println(registration.getDate() + " | hours worked: " + registration.getHoursWorked() + " | Activity: " + registration.getActivity().getName());
        }
        
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Type anything to go back");
        System.out.println("--------------------------------------------------------------------------");
    }

    @Override
    public Object getInput(Scanner console) {
        return console.next();
    }

    @Override
    public void actOnInput(Object input) {
        setNextDisplay(new MenuScreen(getSchedulingSystem()));
    }
}
