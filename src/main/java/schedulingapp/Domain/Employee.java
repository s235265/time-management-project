package schedulingapp.Domain;

import schedulingapp.App.SchedulingSystem;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    private String id;
    private SchedulingSystem schedulingSystem;
    private List<TimeRegistration> timeRegistrations = new ArrayList<>();

    public Employee (String id, SchedulingSystem schedulingSystem) {
        this.id = id;
        this.schedulingSystem = schedulingSystem;
    }
    public Employee(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public double getAssignedHours(){
        double sum = 0;
        for (Activity activity : getActivities()){
            sum += activity.getAssignedHours();
        }
        return sum;
    }

    public void addActivity(Activity activity) throws Exception {
        schedulingSystem.assignEmployeeToActivity(activity, this.getId());
    }

    public ArrayList<Project> getProjects(){
        return schedulingSystem.getProjects(this);
    }

    public ArrayList<Activity> getActivities() {
        return schedulingSystem.getActivities(this);
    }

    public void setSchedulingSystem(SchedulingSystem schedulingSystem) {
        this.schedulingSystem = schedulingSystem;
    }

    public void addTimeRegistration(TimeRegistration timeRegistration) {
        timeRegistrations.add(timeRegistration);
    }

    public List<TimeRegistration> getTimeRegistrations() {
        return timeRegistrations;
    }
}
