package schedulingapp.App;

import io.cucumber.java.bs.A;
import schedulingapp.Domain.*;
import schedulingapp.Domain.Displays.*;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        SchedulingSystem schedulingSystem = new SchedulingSystem();
        addDatabase(schedulingSystem);
        Display currentDisplay = new LogInScreen(schedulingSystem);
        Scanner console = new Scanner(System.in);

        while (true){
            currentDisplay.showScreen();

            Object input = currentDisplay.getInput(console);

            currentDisplay.actOnInput(input);

            currentDisplay = currentDisplay.getNextDisplay();
        }


    }


    public static void addDatabase(SchedulingSystem s) throws Exception {
        Employee huba = new Employee("huba", s);
        s.addEmployee(huba);

        s.addEmployee(new Employee("nick",s));
        s.addEmployee(new Employee("jame",s));
        s.addEmployee(new Employee("john",s));
        s.addEmployee(new Employee("jane",s));
        s.addEmployee(new Employee("mary",s));
        s.addEmployee(new Employee("josh",s));
        s.addEmployee(new Employee("janu",s));
        s.addEmployee(new Employee("chri",s));
        s.addEmployee(new Employee("garf",s));
        StaticActivity sickness = new StaticActivity("Illness", s);
        StaticActivity vacation = new StaticActivity("Vacation", s);
        s.addActivity(sickness);
        s.addActivity(vacation);


        Employee e = new Employee("chra",s);
        s.addEmployee(e);

        Activity gaming = new Activity("Do work",s);
        gaming.setAssignedHours(4.2);
        e.addActivity(gaming);

        Activity gaming2 = new Activity("Pour coffee",s);
        gaming.setAssignedHours(4.2);
        gaming2.setAssignedHours(8);
        e.addActivity(gaming2);





        Project project = new Project("Project 1",s);
        Project project2 = new Project("Project 2",s);

        String[] names = {
                "Pour coffee",
                "Do something else",
                "Check emails",
                "Attend meeting",
                "Write report",
                "Review code",
                "Take a break",
                "Make phone call",
                "Brainstorm ideas",
                "Prepare presentation"
        };

        String[] names2 = {
                "Check notifications",
                "Update status report",
                "Respond to emails",
                "Research topic"
        };

        String[] names3 = {
                "Prepare agenda",
                "File expense reports",
                "Organize files"
        };

        Random random = new Random();

        for (String name : names) {
            Activity activity = new Activity(name, s);
            activity.setAssignedHours(random.nextInt(2,10));
            project.addActivity(activity);
        }


        project.addActivity(gaming);
        project2.addActivity(gaming2);
        Project teach = new Project("Teach",s);
        Activity teach_solid = new Activity("Teach SOLID", s);
        Activity teach_design = new Activity("Teach Design", s);
        teach_design.setDetails("Today we are learning :)");
        teach_solid.setDetails("Today we are learning SOLID principles");
        Activity create_assignment = new Activity("Create Assignments", s);
        teach.addActivity(teach_design);
        teach.addActivity(create_assignment);
        teach.addActivity(teach_solid);
        teach_design.setAssignedHours(2.0);
        teach_solid.setAssignedHours(4);
        create_assignment.setAssignedHours(6.5);

        for (String name : names3) {
            Project newProject = new Project(name, s);
            for (String name2 : names2){
                Activity a = new Activity(name2, s);
                a.setAssignedHours(random.nextInt(2,10));
                newProject.addActivity(a);
            }
            s.addProject(newProject);
        }


        for (String name : names2) {
            Activity activity = new Activity(name, s);
            activity.setAssignedHours(random.nextInt(2,10));
            teach.addActivity(activity);
        }


        Project coffee_break = new Project("Coffee Break",s);
        Activity add_water = new Activity("Add Water", s);
        coffee_break.addActivity(add_water);
        Activity drink_coffee = new Activity("Drink Coffee", s);
        coffee_break.addActivity(drink_coffee);
        add_water.setAssignedHours(4.0);
        drink_coffee.setAssignedHours(3.0);

        s.addProject(teach);
        s.addProject(coffee_break);

        s.assignEmployeeToActivity(drink_coffee, huba);
        s.assignEmployeeToActivity(teach_design, huba);
        s.assignEmployeeToActivity(create_assignment, huba);
        s.assignEmployeeToActivity(add_water, s.findEmployee("garf"));
        String input = "2024-05-01";
        String input2 = "2024-05-02";
        LocalDate date2 = LocalDate.parse(input2);
        LocalDate date = LocalDate.parse(input);
        TimeRegistration drinkCoffeeRegistration = new TimeRegistration(date, 0630, 1230,drink_coffee);
        huba.addTimeRegistration(drinkCoffeeRegistration);

        TimeRegistration teachDesignRegistration = new TimeRegistration(date2, 1430, 1700,teach_design);
        huba.addTimeRegistration(teachDesignRegistration);



        try {
            s.addProject(project);
            s.addProject(project2);
        } catch (Exception ex) {
            System.out.println("whoops");
        }



    }
    public void changeDisplay(Display display){

    }
}
