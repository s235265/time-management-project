package schedulingapp.App;
import schedulingapp.Domain.Activity;
import schedulingapp.Domain.Displays.ActivityScreen;
import schedulingapp.Domain.Employee;
import schedulingapp.Domain.Project;

import java.util.ArrayList;


public class SchedulingSystem {
    private ArrayList<Project> projects = new ArrayList<Project>();
    private ArrayList<Employee> employees = new ArrayList<>();
    private Employee loggedInUser = null;
    private Employee selectedEmployee = null;
    private Project selectedProject = null;
    private Activity selectedActivity = null;
    private ArrayList<Activity> systemActivities = new ArrayList<>();


    public Employee getLoggedInUser() {
        return loggedInUser;
    }


    public ArrayList <Project> getProjects(){
        return projects;
    }

    public void logIn(Employee employee) throws Exception {
        if(employees.contains(employee)){
            this.loggedInUser = employee;
            this.selectedEmployee = employee;
        } else {
            for(Employee e : employees) {
                if(e.getId().equals(employee.getId())){
                    this.loggedInUser = e;
                    this.selectedEmployee = e;
                    return;
                }
        }
    }

        throw new Exception("No employee with that ID exists");
        }

    public void addProject(Project p) throws Exception {
        if (projects.contains(p) || hasProject(p)){
            throw new Exception("Project name already exists");
        }
        p.setSchedulingSystem(this);
        projects.add(p);
    }

    public void setSelectedProject(Project project){
        selectedProject = project;
    }

    public Project getSelectedProject(){
        return selectedProject;
        }

    public void setSelectedEmployee(Employee employee){
        selectedEmployee = employee;
    }

    public Employee getSelectedEmployee(){
        return selectedEmployee;
    }

    public boolean hasProject(Project p) {
        for (Project project : projects) {
            if (project.getName().equals(p.getName())){
                return true;
            }
        }
        return projects.contains(p);
    }

    public boolean hasEmployee(String input) {
        for (Employee employee : employees) {
            if (employee.getId().equals(input)){
                return true;
            }
        }
        return false;
    }

    public void addEmployee(Employee employee) throws Exception {

        if (hasEmployee(employee.getId())){
            throw new Exception("Employee already exists");
        }

        employee.setSchedulingSystem(this);
        employees.add(employee);
    }

    public void logOut() {
            loggedInUser = null;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void assignEmployeeToActivity(Activity activity, Employee employee) throws Exception {

        assert activity != null && employee != null;

        if (activity.getAssignedEmployees().contains(employee)){
            assert activity.getAssignedEmployees().contains(employee);
            throw new Exception("Employee already assigned to activity");
        } else {

        activity.addAssignedEmployee(employee);
        assert activity.getAssignedEmployees().contains(employee);
    }}

    public ArrayList<Project> getProjects(Employee employee) {
        ArrayList<Project> projects = new ArrayList<>();

        for (Project project : this.projects) {
            for (Activity activity : project.getActivites()){
                if (activity.getAssignedEmployees().contains(employee)){
                    if (!projects.contains(project)){
                        projects.add(project);
                    }
                }

            }
        }
        return projects;
    }

    public ArrayList<Activity> getActivities(){
        return systemActivities;
    }

    public ArrayList<Activity> getActivities(Employee employee) {
        ArrayList<Activity> activities = new ArrayList<>();

        for (Project project : this.projects) {
            for (Activity activity : project.getActivites()){
                if (activity.getAssignedEmployees().contains(employee)){
                    activities.add(activity);
                }
            }
        }

        for (Activity activity : systemActivities){
            if (activity.getAssignedEmployees().contains(employee)){
                if (!activities.contains(activity)){
                    activities.add(activity);
                }
            }
        }
        return activities;
    }

    public void setSelectedEmployee(String input) throws Exception {
        Employee employee = findEmployee(input);
        setSelectedEmployee(employee);
    }

    public void addActivity(Activity activity) {
        systemActivities.add(activity);
    }

    public Activity getSelectedActivity() {
        return selectedActivity;
    }

    public void setSelectedActivity(Activity selectedActivity) {
        this.selectedActivity = selectedActivity;
    }

    public void assignEmployeeToActivity(Activity activity, String id) throws Exception {
        // note that this could just as well call assignEmployeeToActivity(activity, findEmployee(id)), but
        // it's used in the report. sooo... gets optimized by the compiler anyways?
        assert activity != null && id.length() == 4;                            //1
        Employee employee = findEmployee(id); // throws exception               //2
        if (activity.getAssignedEmployees().contains(employee)) {               //3
            assert activity.getAssignedEmployees().contains(employee);          //3
            throw new Exception("Employee already assigned to activity");
        } else {
            activity.addAssignedEmployee(employee);
            assert activity.getAssignedEmployees().contains(employee);
        }
    }

    public Employee findEmployee(String inputString) throws Exception {
        for (Employee employee : employees) {
            if (employee.getId().equals(inputString)){
                return employee;
            }
        }
        throw new Exception("No employee with that ID exists");
    }

    public void removeEmployeeFromActivity(String substring, Activity activity) throws Exception {
        Employee employee = findEmployee(substring);
        activity.removeEmployee(employee);
    }

    public Project getProject(Activity activity) {
        for (Project p : projects){
            if (p.getActivites().contains(activity)){
                return p;
            }
        }
        return null;
    }

    public Project findProject(String inputString) throws Exception {
        for (Project project : projects) {
            if (project.getName().equals(inputString)) {
                return project;
            }
        }
        throw new Exception("No project with that name exists");
    }


    public void removeProject(Project project) throws Exception {

        project.getActivites().clear();
        if (!projects.contains(project)){
            throw new Exception("That project doesn't exist");
        }

        projects.remove(project);
        setSelectedProject(null);
        setSelectedActivity(null);
    }

}
